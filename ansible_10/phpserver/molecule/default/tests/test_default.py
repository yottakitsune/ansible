# def test_nginx_installed (host):
#     assert host.package ("httpd"). is_installed
#
#
# def test_os_name(host):
#     assert host.file("/etc/os-release").contains("Alpine Linux v3.11")
# #
# # def test_nginx_running_and_enabled(host):
# #     nginx = host.service("nginx")
# #     assert nginx.is_running
# #     assert nginx.is_enabled
#

def test_nginx_is_installed(host):
    nginx = host.package("nginx")
    assert nginx.is_installed
    assert nginx.version.startswith("1.16.1")
