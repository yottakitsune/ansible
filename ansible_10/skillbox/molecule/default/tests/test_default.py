# import cmd
# import unittest
# import os, platform, sys, sysconfig

#
# class Testing(unittest.TestCase):
#     def test(self):
#         print(sysconfig.get_platform())
#         print(os.system("nginx -v"))
#         # a = 0
#         # b = 0
#         # self.assertEqual(a, b, True)
#         assert "nginx version: nginx/" in os.system("nginx -v")
#         assert "alpine" in sysconfig.get_platform()



import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_os_name(host):
    assert host.file("/etc/os-release").contains("Alpine Linux v3.11")
#
# def test_nginx_running_and_enabled(host):
#     nginx = host.service("nginx")
#     assert nginx.is_running
#     assert nginx.is_enabled


def test_nginx_is_installed(host):
    nginx = host.package("nginx")
    assert nginx.is_installed
    assert nginx.version.startswith("1.16.1")


#
#
def test_nginx_running_and_enabled(host):
    nginx1 = host.service("nginx")
    assert nginx1.is_running
    assert nginx1.is_enabled
#

def test_nginx_is_listening(host):
    assert host.socket('http://127.0.0.1:8080').is_listening